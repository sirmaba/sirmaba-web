from urllib.parse import urlencode

from django.http import HttpResponse
from django.shortcuts import redirect, render, reverse

from form.forms import InterestForm


def interest(request):
    status_code = 200

    if request.method == 'POST':
        form = InterestForm(request.POST)
        if form.is_valid():
            form.save()
            status_code = 201

            return redirect('{url}?{query}'.format(
                url=reverse('web:landing_page'),
                query=urlencode({'form_submit': 'success'})
            ))

        else:
            status_code = 400

    else:
        form = InterestForm()

    return render(request, 'form/interest-form.html',
                  context={'form': form, 'status_code': status_code},
                  status=status_code)
