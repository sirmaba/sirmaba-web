from django.urls import path

from form.views import interest

app_name = 'form'
urlpatterns = [
    path('interest', interest, name='interest'),
]
