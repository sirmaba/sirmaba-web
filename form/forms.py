from django import forms
from form.models import Interest


class InterestForm(forms.ModelForm):
    class Meta:
        model = Interest
        fields = ('name', 'phone', 'address', 'city', 'zip_code', 'motivation')
        labels = {
            'name': 'Nama',
            'phone': 'Nomor Handphone',
            'address': 'Alamat Lengkap',
            'city': 'Kabupaten / Kota',
            'zip_code': 'Kode Pos',
            'motivation': 'Alasan Ingin Bergabung',
        }

        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control my-1'}),
            'phone': forms.TextInput(attrs={'class': 'form-control my-1'}),
            'address': forms.Textarea(attrs={'class': 'form-control my-1'}),
            'city': forms.TextInput(attrs={'class': 'form-control my-1'}),
            'zip_code': forms.TextInput(attrs={'class': 'form-control my-1'}),
            'motivation': forms.Textarea(attrs={'class': 'form-control my-1'}),
        }
