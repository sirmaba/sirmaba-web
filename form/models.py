from django.db import models
from phonenumber_field.modelfields import PhoneNumberField


class Interest(models.Model):
    name = models.CharField(max_length=100)
    phone = PhoneNumberField()

    address = models.TextField()
    city = models.CharField(max_length=50)
    zip_code = models.CharField(max_length=10)

    motivation = models.TextField()
