import json
from collections import defaultdict
from decimal import Decimal

from django.contrib.auth import password_validation
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.db import NotSupportedError
from django.db.models import Sum, F
from django.http import JsonResponse, HttpResponseNotFound
from django.shortcuts import render
from django.utils.dateparse import parse_date

from account import models as account_models
from transaction import views as transaction_views
from transaction.models import TransactionDetail
from transaction.reports import TransactionSummary, WasteTransactionUnit
from transaction.utils import last_transaction, WasteType


@login_required
def home(request):
    user = request.user
    nasabah = account_models.Nasabah.objects.get(user=user)
    bsu = nasabah.bsu
    transaction = last_transaction(nasabah)
    balance = 0 if transaction is None else transaction.balance

    last_savings = get_all_nasabah_transaction(nasabah, limit=3)
    data = {
        'nasabah_name': nasabah.first_name + " " + nasabah.last_name,
        'bsu_name': bsu.name,
        'balance': balance,
        'last_savings': last_savings,
    }
    return render(request, 'main-pages/home.html', data)


@login_required
def deposit_list(request):
    user = request.user
    nasabah = account_models.Nasabah.objects.get(user=user)

    transactions_query = TransactionDetail.objects \
        .filter(account=nasabah, obj__isnull=False, debit__isnull=False) \
        .values('timestamp__date', 'transaction_id') \
        .distinct()

    transactions_by_date_dict = {}
    for tx in transactions_query:
        date = tx.pop('timestamp__date')
        transactions_by_date_dict.setdefault(date, set()).add(tx['transaction_id'])

    transactions_by_date = {}
    years = set()
    for date, transactions in transactions_by_date_dict.items():
        years.add(date.year)
        transactions_by_date[date] = TransactionDetail.objects \
            .filter(account=nasabah,
                    transaction_id__in=transactions,
                    obj__isnull=True,
                    debit__isnull=False) \
            .values('timestamp__date') \
            .annotate(total_income=Sum('debit')) \
            .get()

        transactions_by_date[date]['total_income'] *= Decimal(.9)

    return render(request, 'main-pages/deposit-list.html', {
        'section': 'setoran',
        'transactions': sorted(
            transactions_by_date.values(),
            key=lambda x: x['timestamp__date'],
            reverse=True
        ),
        'years': sorted(
            years,
            reverse=True
        ),
    })


@login_required
def deposit_detail(request, date_str):
    user = request.user
    nasabah = account_models.Nasabah.objects.get(user=user)
    date = parse_date(date_str)

    transactions_dicts = TransactionDetail.objects \
        .filter(account=nasabah, timestamp__date=date) \
        .exclude(obj=None).exclude(credit=None) \
        .values('account', 'obj') \
        .annotate(amount=Sum('credit'),
                  unit=F('unit'),
                  price=Sum(F('credit') * F('curr_rate')),
                  currency=F('side__unit'))

    summary = defaultdict(TransactionSummary)

    waste_dict = {w.id: w for w in WasteType.objects
                  .filter(pk__in={tx['obj'] for tx in transactions_dicts})
                  .select_related('category')}

    for tx in transactions_dicts:
        summary['data'].add_tx(WasteTransactionUnit(waste=waste_dict[tx['obj']], data_dict=tx))

    summary.default_factory = None
    print(summary['data'])
    return render(request, 'main-pages/deposit-detail.html', {
        'section': 'setoran',
        'date': date,
        'summary': summary['data'],
        'fee': Decimal(.1) * summary['data'].total.price,
        'income': Decimal(.9) * summary['data'].total.price,
    })


@login_required
def savings(request):
    user = request.user
    nasabah = account_models.Nasabah.objects.get(user=user)
    savings_query = get_all_nasabah_transaction(nasabah)
    data = {
        'savings': savings_query
    }
    return render(request, 'main-pages/savings.html', data)


@login_required
def profile(request):
    user = request.user
    nasabah = account_models.Nasabah.objects.get(user=user)
    data = get_profile(nasabah)
    data["nasabah_service_url"] = '/nasabah/{}/'.format(data["nasabah_id"])
    return render(request, 'main-pages/profile.html', data)


def get_profile(nasabah):
    contact = type(nasabah).objects.filter(id=nasabah.id).values().first()['contact']
    bsu = nasabah.bsu

    pure_contact = None if contact is None else contact.replace('-', '')
    data = {
        "nasabah_id": nasabah.id,
        "bsu_id": bsu.id,
        "username": nasabah.user.username,
        "password": None,
        "bsu_name": bsu.name,
        "full_name": nasabah.first_name + " " + nasabah.last_name,
        "first_name": nasabah.first_name,
        "last_name": nasabah.last_name,
        "modified_contact": contact,
        "pure_contact": pure_contact,
        "address": nasabah.address,
        "rt": nasabah.rt,
        "rw": nasabah.rw
    }
    return data


@login_required
def settings(request):
    data = {}
    return render(request, 'main-pages/settings.html', data)


def get_all_nasabah_transaction(nasabah, limit=None):
    savings_list = [{
        'transaction': 'initial',
        'balance': 0,
        'debit': 0,
        'date': None,
    }]

    query = transaction_views.TransactionDetail.objects.filter(account=nasabah, obj__isnull=True).reverse()
    for index in range(len(query) - 1, -1, -1):
        if limit is not None:
            if len(savings_list) > limit:
                break

        if savings_list[-1]['transaction'] != query[index].transaction:
            savings_list[-1]['debit'] = savings_list[-1]['balance'] - query[index].balance
            savings_list.append({
                'transaction': query[index].transaction,
                'balance': query[index].balance,
                'debit': None,
                'date': query[index].timestamp.strftime("%d %B %Y"),
            })

    savings_list[-1]['debit'] = savings_list[-1]['balance']
    return savings_list[1:]


@login_required
def nasabah_service(request, nasabah_id):
    if account_models.Nasabah.objects.get(user=request.user).id != nasabah_id:
        return HttpResponseNotFound
    else:
        return JsonResponse(nasabah_api(request, nasabah_id))


"""
This is Nasabah API

Examples usage of API:
GET: /nasabah/ -- Retrieves a list of nasabah
POST: /nasabah/ -- Creates a nasabah by using data from body request (JSON)
PUT: /nasabah/1 -- Updates a given nasabah with id=1 by using data from body request (JSON)
GET: /nasabah/1 -- Retrieves a specific nasabah with id=1
DELETE: /nasabah/1 -- Deletes a given nasabah with id=1

Example of body request for POST or PUT method:
{
    "username": michael2000,
    "password": M1ch@el2001-1,
    "bank_sampah_id": 12,
    "first_name": "Michael",
    "last_name": "Einstein",
    "contact": "+349823721923",
    "rt": 20,
    "rw": 2,
    "address": "Flower 2 Street, Westvintage Residence, Jakarta Selatan, Jakarta, Indonesia, 18083"
}

Note:
1. body request must be JSON String not JSON object. (Use JSON.stringfy(body) to convert that in JavaScript)
"""


@login_required
def nasabah_api(request, nasabah_id=None):
    try:
        response = {}
        data = None
        if request.method == "GET":
            user = request.user
            data = get_nasabah(nasabah_id)
        elif request.method == "POST":
            body = json.loads(request.body)
            data = create_nasabah(body)
        elif request.method == "PUT":
            body = json.loads(request.body)
            data = update_nasabah(body, nasabah_id)
        elif request.method == "DELETE":
            data = delete_nasabah(nasabah_id)
        else:
            raise NotSupportedError("Service does not support {} method".format(request.method))

    except ObjectDoesNotExist as e:
        response = {
            "status": 409,
            "message": "The resource does not exist",
            "problem": str(e)
        }

    except NotSupportedError as e:
        response = {
            "status": 405,
            "message": "Service can't recognize the request method",
            "problem": str(e)
        }
    except ValidationError as e:
        print(e)
        response = {
            "status": 422,
            "message": "Validation Error",
            "problem": list(e)
        }
    except Exception as e:
        response = {
            "status": 500,
            "message": "Internal Error",
            "problem": str(e)
        }
    else:
        response = {
            "status": 200,
            "message": "Success",
            "data": data
        }

    finally:
        return response


def get_nasabah(nasabah_id=None):
    data = None
    if nasabah_id is None:
        data = list_of(account_models.Nasabah.objects.values())
    else:
        nasabah = get_nasabah_from_database(nasabah_id)
        data = type(nasabah).objects.filter(id=nasabah.id).values().first()

    return data


def create_nasabah(body):
    get_bank_sampah_unit_from_database(body["bsu_id"])
    password_validation.validate_password(body['password'])
    user = User.objects.create_user(
        body['username'],
        '',
        body['password'],
    )

    nasabah = account_models.Nasabah(
        user=user,
        bsu_id=body["bsu_id"],
        first_name=get_proper_name(body["first_name"]),
        last_name=get_proper_name(body["last_name"]),
        contact=None if body['contact'] is None else body["contact"],
        rt=body["rt"],
        rw=body["rw"],
        address=body["address"],
    )

    nasabah.full_clean()
    nasabah.save()

    return nasabah.id


def update_nasabah(body, nasabah_id):
    nasabah = get_nasabah_from_database(nasabah_id)
    bsu = get_bank_sampah_unit_from_database(body["bsu_id"])

    user = get_nasabah_from_database(nasabah_id).user
    if user.username != body['username']:
        try:
            User.objects.get(username=body['username'])
            raise ValidationError(['Username has already been taken'])
        except User.DoesNotExist:
            user.username = body['username']

    if body['password'] is not None:
        password_validation.validate_password(body['password'])
        user.set_password(body['password'])

    user.save()

    nasabah.bsu_id = body["bsu_id"]
    nasabah.first_name = get_proper_name(body["first_name"])
    nasabah.last_name = get_proper_name(body["last_name"])
    nasabah.contact = None if body['contact'] is None else body["contact"]
    nasabah.rt = body["rt"]
    nasabah.rw = body["rw"]
    nasabah.address = body["address"]
    nasabah.full_clean()
    nasabah.save()

    return nasabah_id


def delete_nasabah(nasabah_id):
    get_nasabah_from_database(nasabah_id).delete()
    return ""


def list_of(items):
    return [item for item in items]


def get_bank_sampah_unit_from_database(nasabah_id):
    bsu = account_models.BankSampahUnit.objects.get(id=nasabah_id)
    if bsu is None:
        raise ObjectDoesNotExist("Bank sampah with nasabah_id = {} does not exist".format(nasabah_id))
    else:
        return bsu


def get_nasabah_from_database(nasabah_id):
    nasabah = account_models.Nasabah.objects.get(id=nasabah_id)
    if nasabah is None:
        raise ObjectDoesNotExist("Nasabah with nasabah_id = {} does not exist".format(nasabah_id))
    else:
        return nasabah


def get_proper_name(name):
    new_name = ""
    for word in name.split(' '):
        new_name += word.lower().capitalize() + ' '
    return new_name[:-1]
