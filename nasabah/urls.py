from django.urls import path
from nasabah import views

app_name = "nasabah"
urlpatterns = [
    path("<int:nasabah_id>/", views.nasabah_service),
    path("home/", views.home, name='home'),
    path("deposit/", views.deposit_list, name='deposit-list'),
    path("deposit/<str:date_str>/", views.deposit_detail, name='deposit-detail'),
    path("savings/", views.savings, name='savings'),
    path("profile/", views.profile, name='profile'),
    path("settings/", views.settings, name='settings'),
]
