from typing import Dict, Union, Iterable

from django.db import transaction
from django.db.models import Q, Max

from account.models import Nasabah
from transaction.models import Transaction, TransactionDetail
from transaction.reports import TransactionUnit
from waste.models import WasteType
from waste.utils import get_waste_queryset


def last_transaction(account, obj=None) -> Union[TransactionDetail, Iterable[TransactionDetail]]:
    try:
        iter(obj)
    except TypeError:
        obj_iterable = (obj,)
    else:
        obj_iterable = obj

    obj_query = Q(obj__in=obj_iterable)
    if None in obj_iterable:
        obj_query |= Q(obj__isnull=True)

    transactions_id = TransactionDetail.objects.filter(account=account) \
        .values('obj').annotate(latest_id=Max('id')) \
        .values_list('latest_id', flat=True)

    transactions = TransactionDetail.objects.filter(obj_query, id__in=transactions_id) \
        .select_related('obj')

    try:
        iter(obj)
    except TypeError:
        return transactions.last()
    else:
        return transactions


def get_all_balance(account) -> Dict[Union[WasteType, None], TransactionUnit]:
    wastes = list(get_waste_queryset(account))

    transactions = last_transaction(account, wastes + [None])

    return {tx.obj: TransactionUnit(tx.balance, tx.unit) for tx in transactions}


def add_asset(tx_group, account, amount, obj=None, side_ref=None):
    prev_tx = last_transaction(account, obj)

    return TransactionDetail.objects.create(
        transaction=tx_group,
        account=account,
        obj=obj,
        debit=amount if amount > 0 else None,
        credit=abs(amount) if amount < 0 else None,
        unit=obj.unit if obj else account.currency,
        curr_rate=(obj.bsu_price if isinstance(account, Nasabah) else obj.bsi_price) if obj else 1.0,
        prev_ref=prev_tx,
        side_ref=side_ref,
    )


def subs_asset(tx_group, account, amount, obj=None, side_ref=None):
    return add_asset(tx_group, account, -amount, obj, side_ref)


def deposit_waste(tx_group, customer, bank, waste, amount):
    prev_customer_waste_tx = last_transaction(customer, waste)
    prev_customer_money_tx = last_transaction(customer)
    prev_bank_waste_tx = last_transaction(bank, waste)

    customer_waste_tx = TransactionDetail.objects.create(
        transaction=tx_group,
        account=customer,
        obj=waste,
        credit=amount,
        unit=waste.unit,
        curr_rate=waste.bsu_price if isinstance(customer, Nasabah) else waste.bsi_price,
        prev_ref=prev_customer_waste_tx,
    )

    bank_waste_tx = TransactionDetail.objects.create(
        transaction=tx_group,
        account=bank,
        obj=waste,
        debit=amount,
        unit=waste.unit,
        curr_rate=waste.bsi_price,
        prev_ref=prev_bank_waste_tx,
    )

    customer_money_tx = TransactionDetail.objects.create(
        transaction=tx_group,
        account=customer,
        debit=customer_waste_tx.credit * customer_waste_tx.curr_rate,
        unit=customer.currency,
        prev_ref=prev_customer_money_tx,
        side_ref=customer_waste_tx,
    )

    return (
        customer_waste_tx,
        bank_waste_tx,
        customer_money_tx,
    )


@transaction.atomic
def customer_deposit(customer, wastes_with_amount):
    tx_group = Transaction.objects.create()

    for waste in wastes_with_amount:
        add_asset(tx_group, customer, waste.amount, waste)
        deposit_waste(tx_group, customer, customer.bsu, waste, waste.amount)

    if customer.bsu.deposit_cost:
        income = TransactionDetail.objects.filter(transaction=tx_group, obj=None) \
                                  .exclude(debit=None)
        deposit_cost = customer.bsu.deposit_cost * sum(tx.debit for tx in income)

        subs_asset(tx_group, customer, deposit_cost)
        add_asset(tx_group, customer.bsu, deposit_cost)

    return tx_group


@transaction.atomic
def bsu_deposit(bsu, wastes_with_amount):
    tx_group = Transaction.objects.create()

    total_value = sum([waste.amount * waste.bsi_price for waste in wastes_with_amount])
    add_asset(tx_group, bsu.bsi, total_value)

    for waste in wastes_with_amount:
        _, bank_waste_tx, _ = deposit_waste(tx_group, bsu, bsu.bsi, waste, waste.amount)
        subs_asset(tx_group, bsu.bsi, waste.amount * waste.bsi_price, side_ref=bank_waste_tx)

    return tx_group


@transaction.atomic
def customer_withdraw(customer, amount):
    tx_group = Transaction.objects.create()

    subs_asset(tx_group, customer, amount)
    subs_asset(tx_group, customer.bsu, amount)

    return tx_group


@transaction.atomic
def cleanup(account):
    tx_group = Transaction.objects.create()

    balance_dict = get_all_balance(account)
    del balance_dict[None]

    for waste, tx_unit in balance_dict.items():
        if tx_unit.amount:
            subs_asset(tx_group, account, tx_unit.amount, obj=waste)

    return tx_group
