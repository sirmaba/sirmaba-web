from abc import ABCMeta, abstractmethod
from collections import defaultdict
from typing import Iterable

from waste.models import WasteType


class TransactionUnit:
    amount = 0
    unit = None

    def __init__(self, amount=0, unit=None):
        self.amount += amount
        self.unit = self.unit or unit

    def __add__(self, other):
        return TransactionUnit(self.amount + other.amount, self.unit or other.unit)

    def __sub__(self, other):
        return TransactionUnit(self.amount - other.amount, self.unit or other.unit)

    def __bool__(self):
        return self.amount.__bool__()


class WasteTransactionUnit:
    def __init__(self, waste: WasteType = None,
                 waste_unit_obj: TransactionUnit = None,
                 price_unit_obj: TransactionUnit = None,
                 data_dict: dict = None):

        self._waste = waste

        if isinstance(data_dict, dict):
            self._waste_unit = TransactionUnit(data_dict['amount'], data_dict['unit'])
            self._price_unit = TransactionUnit(data_dict['price'], data_dict['currency'])

        elif (isinstance(waste_unit_obj, TransactionUnit) and
              isinstance(price_unit_obj, TransactionUnit)):
            self._waste_unit = waste_unit_obj
            self._price_unit = price_unit_obj

        else:
            self._waste_unit = TransactionUnit(0, None)
            self._price_unit = TransactionUnit(0, None)

    def __add__(self, other):
        return WasteTransactionUnit(waste=self.waste or other.waste,
                                    waste_unit_obj=self.waste_unit + other.waste_unit,
                                    price_unit_obj=self.price_unit + other.price_unit)

    @property
    def waste_unit(self):
        return self._waste_unit

    @property
    def price_unit(self):
        return self._price_unit

    @property
    def waste(self):
        return self._waste

    @property
    def amount(self):
        return self._waste_unit.amount

    @property
    def unit(self):
        return self._waste_unit.unit

    @property
    def price(self):
        return self._price_unit.amount

    @property
    def currency(self):
        return self._price_unit.unit


class AbstractSummary(defaultdict, metaclass=ABCMeta):
    def __init__(self, *args, **kwargs):
        transactions = kwargs.pop('transactions', None)

        super().__init__(*args, **kwargs)
        if transactions:
            for tx in transactions:
                self.add_tx(tx)

    def __getitem__(self, key):
        try:
            key_attr = getattr(self, key)
            try:
                return key_attr()
            except TypeError:
                return key_attr

        except (TypeError, AttributeError):
            return self.setdefault(key, self.default_factory())

    @abstractmethod
    def add_tx(self, tx):
        pass

    @property
    @abstractmethod
    def total(self):
        pass


class CategoryTransactionSummary(AbstractSummary):
    def __init__(self, transactions: Iterable[WasteTransactionUnit] = None):
        super().__init__(WasteTransactionUnit, transactions=transactions)

    def add_tx(self, tx):
        self[tx.waste] += tx

    @property
    def total(self):
        return sum(self.values(), WasteTransactionUnit())


class TransactionSummary(AbstractSummary):
    def __init__(self, transactions: Iterable[WasteTransactionUnit] = None):
        super().__init__(CategoryTransactionSummary, transactions=transactions)

    def add_tx(self, tx):
        self[tx.waste.category].add_tx(tx)

    @property
    def total(self):
        return sum((tx.total for tx in self.values()), WasteTransactionUnit())
