from django.core.validators import MinValueValidator
from django.db import models
from djmoney.models.fields import CurrencyField

from account.models import BankAccount, BankSampahUnit
from waste.models import WasteType


class Transaction(models.Model):
    timestamp = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return 'transaction {id}'.format(id=self.id)


class TransactionDetail(models.Model):
    transaction = models.ForeignKey(Transaction, on_delete=models.PROTECT, related_name='details')
    timestamp = models.DateTimeField()
    account = models.ForeignKey(BankAccount, on_delete=models.DO_NOTHING)
    obj = models.ForeignKey(WasteType, on_delete=models.DO_NOTHING, null=True)
    debit = models.DecimalField(max_digits=19, decimal_places=4, null=True)
    credit = models.DecimalField(max_digits=19, decimal_places=4, null=True)
    balance = models.DecimalField(max_digits=19, decimal_places=4, validators=[MinValueValidator(0)])
    unit = CurrencyField(max_length=32)
    curr_rate = models.DecimalField(max_digits=19, decimal_places=4, default=1.0)
    prev_ref = models.OneToOneField('self', on_delete=models.PROTECT, null=True, related_name='next')
    side_ref = models.OneToOneField('self', on_delete=models.PROTECT, null=True, related_name='side')

    def save(self, *args, **kwargs):
        self.balance = (self.prev_ref.balance if self.prev_ref else 0) + (self.debit or 0) - (self.credit or 0)
        self.timestamp = self.transaction.timestamp
        return super().save(*args, **kwargs)


class PickupRequest(models.Model):
    transaction = models.OneToOneField(Transaction, on_delete=models.DO_NOTHING, null=True)
    bsu = models.ForeignKey(BankSampahUnit, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "{}: {}".format(self.bsu, self.created_at.date())


class PickupWasteAmount(models.Model):
    request = models.ForeignKey(PickupRequest, on_delete=models.CASCADE, related_name='wasteamount_set')
    waste = models.ForeignKey(WasteType, on_delete=models.DO_NOTHING)
    amount = models.DecimalField(max_digits=19, decimal_places=4)

    def __str__(self):
        return "{} - {} {}".format(self.request, self.waste, self.amount)
