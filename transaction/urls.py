from django.urls import path
from transaction import views

app_name = 'transaction'
urlpatterns = [
    path('deposit/', views.deposit, name='deposit'),
    path('deposit/add/', views.add_deposit, name='deposit-add'),
    path('deposit/account/<int:customer_id>/', views.account_deposit, name='account-deposit'),
    path('deposit/summary/<int:tx_id>/', views.deposit_summary, name='deposit-summary'),
    path('deposit/detail/<str:date_str>/', views.deposit_detail, name='deposit-detail'),
    path('deposit-list/', views.deposit_list, name='deposit-list'),
    path('deposit-list/<str:date_str>/detail/', views.bsi_deposit_detail, name='bsi-deposit-detail'),
    path('pickup/', views.pickup, name='pickup'),
    path('pickup/call/', views.call, name='call'),
    path('pickup/history/<int:pickup_id>/', views.history, name='history'),
    path('pickup-list/', views.pickup_list, name='pickup-list'),
    path('pickup/confirm/<int:pickup_id>/', views.pickup_confirmation, name='pickup-confirm'),
]
