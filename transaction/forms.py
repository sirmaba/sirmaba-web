from django import forms
from django.forms import formset_factory

from waste.models import WasteType
from waste.utils import get_waste_queryset


class WasteDepositForm(forms.Form):
    class WasteChoiceField(forms.ModelChoiceField):
        def label_from_instance(self, obj):
            return "{category}: {name}".format(category=str(obj.category).title(), name=str(obj.name).title())

    waste = WasteChoiceField(WasteType.objects, label='Jenis Sampah', empty_label='')
    amount = forms.DecimalField(label='Jumlah', max_digits=19, decimal_places=4)

    def __init__(self, customer, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['waste'].queryset = get_waste_queryset(customer)
        self.fields['waste'].widget.attrs['class'] = 'form-control'
        self.fields['amount'].widget.attrs['class'] = 'form-control'


WasteDepositFormset = formset_factory(WasteDepositForm)
