from django.contrib import admin
from transaction.models import Transaction, TransactionDetail, PickupRequest, PickupWasteAmount


@admin.register(Transaction)
class TransactionAdmin(admin.ModelAdmin):
    list_display = ('id', 'timestamp')
    list_display_links = None
    readonly_fields = tuple(f.name for f in Transaction._meta.get_fields())

    def get_queryset(self, request):
        return super().get_queryset(request)

    def has_change_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


@admin.register(TransactionDetail)
class TransactionDetailAdmin(admin.ModelAdmin):
    list_display = ('id', 'transaction', 'timestamp', 'account',
                    'obj', 'debit', 'credit', 'balance', 'unit',
                    'curr_rate', 'prev_ref', 'side_ref')
    list_display_links = None
    readonly_fields = tuple(f.name for f in TransactionDetail._meta.get_fields())

    def get_queryset(self, request):
        return super().get_queryset(request)

    def has_change_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


@admin.register(PickupRequest)
class PickupRequestAdmin(admin.ModelAdmin):
    pass


@admin.register(PickupWasteAmount)
class PickupWasteAmountAdmin(admin.ModelAdmin):
    pass
