from collections import defaultdict

from django.contrib.auth.decorators import login_required
from django.db import transaction
from django.db.models import Count, Sum, F
from django.shortcuts import render, redirect
from django.utils.dateparse import parse_date

from account.models import Nasabah, BankSampahUnit, BankSampahInduk
from transaction.forms import WasteDepositFormset
from transaction.models import TransactionDetail, PickupRequest, PickupWasteAmount
from transaction.reports import TransactionSummary, WasteTransactionUnit, TransactionUnit
from transaction.utils import customer_deposit, bsu_deposit
from waste.models import WasteType
from waste.utils import get_waste_queryset


@login_required
def deposit(request):
    user = request.user
    bsu = BankSampahUnit.objects.get(user=user)

    transactions_query = TransactionDetail.objects \
        .filter(account=bsu, obj__isnull=False, debit__isnull=False) \
        .values('timestamp__date', 'transaction_id') \
        .distinct()

    transactions_by_date_dict = {}
    for tx in transactions_query:
        date = tx.pop('timestamp__date')
        transactions_by_date_dict.setdefault(date, set()).add(tx['transaction_id'])

    transactions_by_date = {}
    for date, transactions in transactions_by_date_dict.items():
        transactions_by_date[date] = TransactionDetail.objects \
            .filter(transaction_id__in=transactions, obj=None) \
            .exclude(account=bsu) \
            .values('timestamp__date') \
            .annotate(nr_of_account=Count('account', distinct=True),
                      total_income=Sum('debit')) \
            .get()
        print(transactions_by_date[date])

    return render(request, 'transaction/deposit/deposit.html', {
        'section': 'setoran',
        'transactions': sorted(
            transactions_by_date.values(),
            key=lambda x: x['timestamp__date'],
            reverse=True
        )
    })


@login_required
def add_deposit(request):
    user = request.user
    bsu = BankSampahUnit.objects.get(user=user)

    customers = Nasabah.objects.filter(bsu=bsu)

    return render(request, 'transaction/deposit/add-deposit.html', {'section': 'setoran', 'customers': customers})


@login_required
def account_deposit(request, customer_id):
    user = request.user
    bsu = BankSampahUnit.objects.get(user=user)

    customer = Nasabah.objects.get(id=customer_id, bsu=bsu)

    if request.method == 'POST':
        formset = WasteDepositFormset(request.POST, form_kwargs={'customer': customer})
        if formset.is_valid():
            wastes = []
            for form in formset:
                waste = form.cleaned_data.get('waste')
                waste.amount = form.cleaned_data.get('amount')
                wastes.append(waste)
            transaction = customer_deposit(customer, wastes)
            return redirect('transaction:deposit-summary', tx_id=transaction.id)
    else:
        formset = WasteDepositFormset(form_kwargs={'customer': customer})

    return render(request, 'transaction/deposit/account-deposit.html', {
        'section': 'setoran',
        'account': customer,
        'formset': formset,
    })


@login_required
def deposit_summary(request, tx_id):
    user = request.user
    bsu = BankSampahUnit.objects.get(user=user)

    if not TransactionDetail.objects.filter(transaction_id=tx_id, account=bsu).exists():
        raise TransactionDetail.DoesNotExist('Transaction not found.')

    account_id = TransactionDetail.objects \
        .filter(transaction_id=tx_id).exclude(account=bsu) \
        .values_list('account', flat=True).distinct().get()

    customer = Nasabah.objects.get(id=account_id)

    transactions = TransactionDetail.objects \
        .filter(transaction_id=tx_id, account=customer) \
        .exclude(obj=None).exclude(credit=None) \
        .values('id', 'obj') \
        .annotate(amount=Sum('credit'),
                  unit=F('unit'),
                  price=Sum(F('credit') * F('curr_rate')),
                  currency=F('side__unit'))

    waste = {w.id: w for w in WasteType.objects
             .filter(pk__in={tx['obj'] for tx in transactions})
             .select_related('category')}

    transactions_units = []
    for tx in transactions:
        tx["waste"] = waste[tx["obj"]]

        transactions_units.append(WasteTransactionUnit(waste=tx["waste"], data_dict=tx))

    summary = TransactionSummary(transactions_units)

    try:
        deposit_cost = TransactionDetail.objects \
            .filter(transaction_id=tx_id, account=customer, obj=None) \
            .exclude(credit=None) \
            .annotate(amount=F('credit')) \
            .values('amount', 'unit') \
            .last()

        summary.cost = TransactionUnit(**deposit_cost)
        summary.customer_income = summary.total.price_unit - summary.cost
    except TypeError as e:
        pass

    return render(request, 'transaction/deposit/deposit-summary.html', {
        'section': 'setoran',
        'customer': customer,
        'summary': summary,
    })


@login_required
def deposit_detail(request, date_str):
    user = request.user
    bsu = BankSampahUnit.objects.get(user=user)
    date = parse_date(date_str)

    tx_groups = TransactionDetail.objects \
        .filter(timestamp__date=date, account=bsu) \
        .exclude(obj=None).exclude(debit=None) \
        .values_list('transaction_id', flat=True) \
        .distinct()

    transactions_dicts = TransactionDetail.objects \
        .filter(transaction_id__in=tx_groups) \
        .exclude(account=bsu).exclude(obj=None).exclude(credit=None) \
        .values('account', 'obj') \
        .annotate(amount=Sum('credit'),
                  unit=F('unit'),
                  price=Sum(F('credit') * F('curr_rate')),
                  currency=F('side__unit'))

    deposit_cost_dicts = TransactionDetail.objects \
        .filter(transaction_id__in=tx_groups, obj=None) \
        .exclude(account=bsu).exclude(credit=None) \
        .values('account', 'credit', 'unit')

    nasabah_dict = {n.id: n for n in Nasabah.objects
                    .filter(pk__in={tx['account'] for tx in transactions_dicts})}

    waste_dict = {w.id: w for w in WasteType.objects
                  .filter(pk__in={tx['obj'] for tx in transactions_dicts})
                  .select_related('category')}

    transactions_units = []
    for tx in transactions_dicts:
        tx['nasabah'] = nasabah_dict[tx['account']]
        tx['waste'] = waste_dict[tx['obj']]

        transactions_units.append(WasteTransactionUnit(waste=tx['waste'], data_dict=tx))

    total_cost = TransactionUnit()
    for tx in deposit_cost_dicts:
        tx['nasabah'] = nasabah_dict[tx['account']]
        total_cost += TransactionUnit(amount=tx['credit'], unit=tx['unit'])

    summary = TransactionSummary(transactions_units)
    if total_cost:
        summary.cost = total_cost
        summary.customer_income = summary.total.price_unit - total_cost

    summary_nasabah = defaultdict(TransactionSummary)
    for tx in transactions_dicts:
        summary_nasabah[tx['nasabah']].add_tx(WasteTransactionUnit(waste=tx['waste'], data_dict=tx))
        summary_nasabah[tx['nasabah']].cost = TransactionUnit()

    cost_nasabah = defaultdict(TransactionUnit)
    for tx in deposit_cost_dicts:
        cost_nasabah[tx['nasabah']] += TransactionUnit(tx['credit'], tx['unit'])

    for customer, cost in cost_nasabah.items():
        summary_nasabah[customer].cost = cost
        summary_nasabah[customer].customer_income = summary_nasabah[customer].total.price_unit - cost

    summary_nasabah.default_factory = None

    return render(request, 'transaction/deposit/deposit-detail.html', {
        'section': 'setoran',
        'date': date,
        'summary': summary,
        'summary_nasabah': summary_nasabah,
    })


def deposit_list(request):
    context = {
        'section': 'rekapitulasi',
    }
    return render(request, 'transaction/deposit/bsi/rekapitulasi.html', context)


def bsi_deposit_detail(request, date_str):
    context = {
        'section': 'rekapitulasi',
    }
    return render(request, 'transaction/deposit/bsi/rekapitulasi-detail.html', context)


@login_required
def pickup(request):
    user = request.user
    bsu = BankSampahUnit.objects.get(user=user)

    waste_dict = get_waste_queryset(bsu).in_bulk()

    pickup_wait_dict = PickupRequest.objects.filter(bsu=bsu, transaction_id=None) \
                                    .in_bulk()

    pickup_done_dict = PickupRequest.objects.filter(bsu=bsu) \
                                    .exclude(transaction_id=None) \
                                    .in_bulk(field_name='transaction_id')

    pickup_amount_list = PickupWasteAmount.objects.filter(request_id__in=pickup_wait_dict.keys())
    can_call_pickup = True

    for pickup_request in pickup_amount_list:
        pr_id = pickup_request.request_id
        curr_price = pickup_request.amount * waste_dict[pickup_request.waste_id].bsi_price
        if bsu == pickup_request.request.bsu:
            can_call_pickup = False
        try:
            pickup_wait_dict[pr_id].value += curr_price
        except AttributeError:
            pickup_wait_dict[pr_id].value = curr_price

    transactions = TransactionDetail.objects.filter(transaction_id__in=pickup_done_dict.keys(),
                                                    obj=None, account=bsu).exclude(debit=None) \
                                    .values('transaction_id') \
                                    .annotate(value=Sum('debit'))

    for tx in transactions:
        pickup_done_dict[tx['transaction_id']].value = tx['value']

    context = {
        'section': 'penjemputan',
        'can_call_pickup': can_call_pickup,
        'pickup_request': [*pickup_wait_dict.values(), *pickup_done_dict.values()],
    }

    return render(request, 'transaction/pickup/bsu/pickup.html', context)


@login_required
def call(request):
    user = request.user
    bsu = BankSampahUnit.objects.get(user=user)

    if PickupRequest.objects.filter(bsu=bsu, transaction=None).exists():
        return redirect('transaction:pickup')
        # raise AssertionError('You have existing pickup request,'
        #                      'please edit instead of create a new one')

    if request.method == 'POST':
        formset = WasteDepositFormset(request.POST, form_kwargs={'customer': bsu})
        if formset.is_valid():
            with transaction.atomic():
                pickup_request = PickupRequest.objects.create(bsu=bsu)
                for form in formset:
                    PickupWasteAmount.objects.create(
                        request=pickup_request,
                        waste=form.cleaned_data.get('waste'),
                        amount=form.cleaned_data.get('amount'),
                    )
            return redirect('transaction:pickup')
    else:
        formset = WasteDepositFormset(form_kwargs={'customer': bsu})

    context = {
        'section': 'penjemputan',
        'formset': formset,
    }

    return render(request, 'transaction/pickup/bsu/call.html', context)


@login_required
def history(request, pickup_id):
    user = request.user
    bsu = BankSampahUnit.objects.get(user=user)

    if not PickupRequest.objects.filter(id=pickup_id, bsu=bsu).exists():
        raise PickupRequest.DoesNotExist('Transaction not found.')

    pickup_request = PickupRequest.objects.get(id=pickup_id, bsu=bsu)

    summary = None
    formset = None
    date = None

    if pickup_request.transaction_id:
        transactions = TransactionDetail.objects \
            .filter(transaction_id=pickup_request.transaction_id, account=bsu) \
            .exclude(obj=None).exclude(credit=None) \
            .values('id', 'obj') \
            .annotate(amount=F('credit'),
                      unit=F('unit'),
                      price=F('credit') * F('curr_rate'),
                      currency=F('side__unit'))

        waste = {w.id: w for w in WasteType.objects
                 .filter(pk__in={tx['obj'] for tx in transactions})
                 .select_related('category')}

        transactions_units = []
        for tx in transactions:
            tx["waste"] = waste[tx["obj"]]

            transactions_units.append(WasteTransactionUnit(waste=tx["waste"], data_dict=tx))

        summary = TransactionSummary(transactions_units)
        date = pickup_request.updated_at
    else:
        if request.method == 'POST':
            formset = WasteDepositFormset(request.POST, form_kwargs={'customer': bsu})
            if formset.is_valid():
                with transaction.atomic():
                    pickup_request = PickupRequest.objects.create(bsu=bsu)
                    PickupWasteAmount.objects.filter(request=pickup_request).delete()
                    for form in formset:
                        PickupWasteAmount.objects.create(
                            request=pickup_request,
                            waste=form.cleaned_data.get('waste'),
                            amount=form.cleaned_data.get('amount'),
                        )
                return redirect('transaction:pickup')
        else:
            waste_amount = PickupWasteAmount.objects.filter(request_id=pickup_id)
            formset = WasteDepositFormset(
                initial=[{'waste': wa.waste, 'amount': wa.amount} for wa in waste_amount],
                form_kwargs={'customer': bsu},
            )
            formset.extra = 0

    context = {
        'section': 'penjemputan',
        'summary': summary,
        'formset': formset,
        'date': date,
    }
    return render(request, 'transaction/pickup/bsu/history.html', context)


@login_required
def pickup_list(request):
    user = request.user
    bsi = BankSampahInduk.objects.get(user=user)

    bsu_list = BankSampahUnit.objects.filter(bsi=bsi)

    pickup_wait = PickupRequest.objects.filter(bsu__bsi=bsi, transaction=None).in_bulk()

    pickup_amount = PickupWasteAmount.objects.select_related('waste') \
        .filter(request_id__in=pickup_wait.keys())

    pickup_wait_units = {}
    for waste_amount in pickup_amount:
        data = {
            'amount': waste_amount.amount,
            'unit': waste_amount.waste.unit,
            'price': waste_amount.amount * waste_amount.waste.bsi_price,
            'currency': bsi.currency,
        }
        pickup_wait_units.setdefault(waste_amount.request_id, []) \
            .append(WasteTransactionUnit(waste=waste_amount.waste,
                                         data_dict=data))

    for pickup_id, units in pickup_wait_units.items():
        pickup_wait[pickup_id].summary = TransactionSummary(units)

    pickup_done = PickupRequest.objects.filter(bsu__bsi=bsi) \
        .exclude(transaction=None) \
        .in_bulk(field_name='transaction_id')

    transactions = TransactionDetail.objects \
        .filter(transaction_id__in=pickup_done.keys(), account__in=bsu_list) \
        .exclude(obj=None).exclude(credit=None) \
        .values('id', 'obj', 'transaction_id') \
        .annotate(amount=F('credit'),
                  unit=F('unit'),
                  price=F('credit') * F('curr_rate'),
                  currency=F('side__unit'))

    waste = WasteType.objects \
        .filter(pk__in={tx['obj'] for tx in transactions}) \
        .select_related('category') \
        .in_bulk()

    pickup_done_units = {}
    for tx in transactions:
        tx["waste"] = waste[tx["obj"]]

        pickup_done_units.setdefault(tx['transaction_id'], []) \
            .append(WasteTransactionUnit(waste=tx["waste"], data_dict=tx))

    for pickup_id, units in pickup_done_units.items():
        pickup_done[pickup_id].summary = TransactionSummary(units)

    context = {
        'section': 'penjemputan',
        'pickup_requests': [*pickup_wait.values(), *pickup_done.values()],
    }
    return render(request, 'transaction/pickup/bsi/pickup-list.html', context)


@login_required
def pickup_confirmation(request, pickup_id):
    user = request.user
    bsi = BankSampahInduk.objects.get(user=user)

    pickup_request = PickupRequest.objects.get(id=pickup_id, bsu__bsi=bsi)
    bsu = pickup_request.bsu

    wastes = []
    for waste_amount in pickup_request.wasteamount_set.all():
        waste = waste_amount.waste
        waste.amount = waste_amount.amount

        wastes.append(waste)

    pickup_request.transaction = bsu_deposit(bsu, wastes)
    pickup_request.save()

    return redirect('transaction:pickup-list')
