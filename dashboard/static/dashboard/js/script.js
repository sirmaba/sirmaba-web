$(document).ready(function () {
    $('#sidebarCollapse').on('click', function(){
        $('.overlay').animate({width: "toggle"});
        $('#sidebar').animate({width: "toggle"});
    });

    $('.overlay').on('click', function(){
        $('#sidebar').animate({width: "toggle"});
        $('.overlay').animate({width: "toggle"});
    });
});
