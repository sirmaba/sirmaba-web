from django.urls import path
from dashboard import views

app_name = 'dashboard'
urlpatterns = [
    # BSU dashboard
    path('', views.index, name='index'),
    path('report/', views.report, name='report'),
    path('settings/', views.settings, name='settings'),
    path('profile/', views.profile, name='profile'),
    path('profile/edit/', views.profile_edit, name='profile-edit'),
    path('profile/admin/', views.profile_admin, name='admin-edit'),
    # BSI dashboard
    path('bsi-dashboard/', views.bsi_dashboard, name='bsi-dashboard'),
    path('bsu-create/', views.bsu_create, name='bsu-create'),
    path('bsu-list/', views.bsu_list, name='bsu-list'),
    path('bsu-detail/<int:bsu_id>/', views.bsu_detail, name='bsu-detail'),
    path('bsu-detail/<int:bsu_id>/edit/', views.bsu_edit, name='bsu-edit'),
    path('bsu-detail/<int:bsu_id>/report/<str:date_str>/', views.bsu_report_detail, name='bsu-report-detail'),
    path('bsu/<int:bsu_id>/nasabah/<nasabah_id>/', views.bsu_nasabah_service),
    path('bsu/<int:bsu_id>/nasabah/', views.bsu_nasabah_service),
    # Nasabah dashboard
    path('nasabah/list/', views.nasabah_list, name="nasabah-list"),
    path('nasabah/new/', views.nasabah_form, name="nasabah-form"),
    path('nasabah/<int:nasabah_id>/profile/', views.nasabah_profile, name="nasabah-profile"),
]
