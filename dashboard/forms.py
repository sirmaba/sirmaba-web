from django.forms import ModelForm
from account.models import BankSampahUnit
from django.forms import TextInput, NumberInput, Textarea


class BSUCreationForm(ModelForm):
    class Meta:
        model = BankSampahUnit
        fields = ('name',)
        labels = {
            'name': 'Nama BSU',
        }


class BSUEditForm(ModelForm):
    class Meta:
        model = BankSampahUnit
        fields = (
            'name', 'est_year', 'pic', 'contact', 'address',
            'decree_no', 'decree_issuer'
        )
        labels = {
            'name': 'Nama BSU',
            'est_year': 'Tahun Berdiri',
            'pic': 'Penanggung Jawab',
            'contact': 'Nomor Telepon/HP',
            'address': 'Alamat BSU',
            'decree_no': 'No. SK',
            'decree_issuer': 'Yang mengeluarkan SK',
        }

        widgets = {
            'name': TextInput(attrs={'class': 'form-control'}),
            'est_year': NumberInput(attrs={'class': 'form-control'}),
            'pic': TextInput(attrs={'class': 'form-control'}),
            'contact': TextInput(attrs={'class': 'form-control'}),
            'address': Textarea(attrs={'class': 'form-control'}),
            'decree_no': TextInput(attrs={'class': 'form-control'}),
            'decree_issuer': TextInput(attrs={'class': 'form-control'}),
        }
