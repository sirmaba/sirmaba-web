import decimal
from collections import defaultdict

from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth.forms import UserCreationForm
from django.db.models import Count, Sum, F
from django.http import JsonResponse, HttpResponse
from django.shortcuts import render
from django.utils.dateparse import parse_date
from django.utils.timezone import localdate

from account.models import BankSampahInduk, BankSampahUnit, Nasabah
from dashboard.forms import BSUCreationForm, BSUEditForm
from nasabah import views as nasabah_views
from transaction.models import TransactionDetail
from transaction.reports import WasteTransactionUnit, TransactionSummary
from waste.models import WasteType


@login_required
def index(request):
    user = request.user
    bsu = BankSampahUnit.objects.get(user=user)

    waste_types = WasteType.objects.filter(category__bsi = bsu.bsi)

    return render(
        request, 
        'dashboard/bsu/index.html', 
        {
            'bsu': bsu, 
            'section': 'beranda',
            'waste_types': waste_types,
        }
    )


@login_required
def report(request):
    user = request.user
    bsu = BankSampahUnit.objects.get(user=user)

    transactions = TransactionDetail.objects.filter(account=bsu).exclude(debit=None)

    total_income = 0
    total_waste = 0
    waste_debit = defaultdict(decimal.Decimal)
    timeline_income = defaultdict(decimal.Decimal)

    summary_units = []

    for tx in transactions:
        if tx.obj is None:
            total_income += tx.debit
            timeline_income[localdate(tx.timestamp).replace(day=1)] += tx.debit
        else:
            total_waste += tx.debit
            waste_debit[tx.obj] += tx.debit

            summary_units.append(WasteTransactionUnit(waste=tx.obj, data_dict={
                'amount': tx.debit,
                'unit': tx.unit,
                'price': tx.debit * tx.curr_rate,
                'currency': 'IDR',
            }))

    waste_debit.default_factory = None
    timeline_income.default_factory = None

    summary = TransactionSummary(summary_units)

    class DailyReport:
        total_weight = 0
        nasabah_set = set()
        bsu_deposit_income = 0

        customer_deposit_tax = 0
        customer_deposit_valued = 0

        customer_deposit_margin = 0
        bsu_customer_margin = 0
        net_profit = 0

    daily_report = defaultdict(DailyReport)

    for tx in transactions:
        if tx.obj is None:
            if tx.side_ref is None:
                daily_report[localdate(tx.timestamp)].customer_deposit_tax += tx.debit
            else:
                daily_report[localdate(tx.timestamp)].bsu_deposit_income += tx.debit
        else:
            daily_report[localdate(tx.timestamp)].total_weight += tx.debit

    customer_transactions = TransactionDetail.objects \
        .filter(transaction_id__in=transactions.values_list('transaction_id', flat=True)) \
        .filter(obj=None) \
        .exclude(account=bsu).exclude(debit=None)

    for tx in customer_transactions:
        daily_report[localdate(tx.timestamp)].nasabah_set.add(tx.account)
        daily_report[localdate(tx.timestamp)].customer_deposit_valued += tx.debit

    for data in daily_report.values():
        data.customer_deposit_margin = data.customer_deposit_valued - data.customer_deposit_tax
        data.bsu_customer_margin = data.bsu_deposit_income - data.customer_deposit_valued
        data.net_profit = data.bsu_customer_margin + data.customer_deposit_tax

    daily_report.default_factory = None

    return render(request, 'dashboard/bsu/report.html', {
        'section': 'laporan',
        'total_income': total_income,
        'total_waste': total_waste,
        'waste_debit': waste_debit,
        'timeline_income': sorted(timeline_income.items()),
        'summary': summary,
        'daily_report': sorted(daily_report.items(), reverse=True),
    })


@login_required
def settings(request):
    return render(request, 'dashboard/bsu/settings.html')


@login_required
def profile(request):
    user = request.user
    bsu = BankSampahUnit.objects.get(user=user)

    return render(request, 'dashboard/bsu/profile.html', {'bsu': bsu})


@login_required
def profile_edit(request):
    status_code = 200

    user = request.user
    bsu = BankSampahUnit.objects.get(user=user)

    if request.method == 'POST':
        profile_form = BSUEditForm(request.POST, instance=bsu)

        if profile_form.is_valid():
            bsu = profile_form.save()
        else:
            status_code = 400
    else:
        profile_form = BSUEditForm(instance=bsu)

    context = {
        'status_code': status_code,
        'profile_form': profile_form,
    }

    return render(request, 'dashboard/bsu/profile-edit.html', context, status=status_code)


@login_required
def profile_admin(request):
    status_code = 200

    user = request.user
    bsu = BankSampahUnit.objects.get(user=user)

    if request.method == 'POST':
        admin_form = UserCreationForm(request.POST, instance=bsu.user)

        if admin_form.is_valid():
            admin = admin_form.save()
            update_session_auth_hash(request, admin)
        else:
            status_code = 400
    else:
        admin_form = UserCreationForm(instance=bsu.user)

    context = {
        'status_code': status_code,
        'admin_form': admin_form,
    }

    return render(request, 'dashboard/bsu/admin-edit.html', context, status=status_code)


@permission_required('account.add_banksampah')
def bsu_create(request):
    status_code = 200
    user = request.user

    if request.method == 'POST':
        bsu_admin_form = UserCreationForm(request.POST)
        bsu_data_form = BSUCreationForm(request.POST)

        if bsu_admin_form.is_valid() and bsu_data_form.is_valid():
            admin = bsu_admin_form.save()
            user_bsi = BankSampahInduk.objects.get(user=user)

            bsu = bsu_data_form.save(commit=False)
            bsu.user = admin
            bsu.bsi = user_bsi
            bsu.save()

            status_code = 201
        else:
            status_code = 400
    else:
        bsu_admin_form = UserCreationForm()
        bsu_data_form = BSUCreationForm()

    context = {
        'bsu_admin_form': bsu_admin_form,
        'bsu_data_form': bsu_data_form,
        'status_code': status_code,
        'section': 'bsu',
    }

    return render(request, 'dashboard/bsi/bsu-create.html', context)


@login_required
def bsu_list(request):
    user = request.user

    if user.is_authenticated:
        bank_sampah = BankSampahInduk.objects.get(user=user)

        if bank_sampah:
            bsu_query = BankSampahUnit.objects.filter(bsi=bank_sampah)

    return render(request, 'dashboard/bsi/bsu-list.html', {'bsu_list': bsu_query, 'section': 'bsu'})


@login_required
def bsu_detail(request, bsu_id):
    user = request.user

    bsu = BankSampahUnit.objects.filter(bsi__user=user).get(id=bsu_id)

    transactions_query = TransactionDetail.objects \
        .filter(account=bsu, obj__isnull=False, debit__isnull=False) \
        .values('timestamp__date', 'transaction_id') \
        .distinct()

    transactions_by_date_dict = {}
    for tx in transactions_query:
        date = tx.pop('timestamp__date')
        transactions_by_date_dict.setdefault(date, set()).add(tx['transaction_id'])

    transactions_by_date = {}
    for date, transactions in transactions_by_date_dict.items():
        transactions_by_date[date] = TransactionDetail.objects \
            .filter(transaction_id__in=transactions, obj=None) \
            .exclude(account=bsu) \
            .values('timestamp__date') \
            .annotate(nr_of_account=Count('account', distinct=True),
                      total_income=Sum('debit')) \
            .get()

    return render(request, 'dashboard/bsi/bsu-detail.html', {
        'section': 'bsu',
        'bsu': bsu,
        'transactions': sorted(
            transactions_by_date.values(),
            key=lambda x: x['timestamp__date'],
            reverse=True
        )
    })


@login_required
def bsu_report_detail(request, date_str, bsu_id):
    user = request.user
    bsu = BankSampahUnit.objects.filter(bsi__user=user).get(id=bsu_id)
    date = parse_date(date_str)
    print(date)

    tx_groups = TransactionDetail.objects \
        .filter(timestamp__date=date, account=bsu) \
        .exclude(obj=None).exclude(debit=None) \
        .values_list('transaction_id', flat=True) \
        .distinct()

    transactions_dicts = TransactionDetail.objects \
        .filter(transaction_id__in=tx_groups) \
        .exclude(account=bsu).exclude(obj=None).exclude(credit=None) \
        .values('account', 'obj') \
        .annotate(amount=Sum('credit'),
                  unit=F('unit'),
                  price=Sum(F('credit') * F('curr_rate')),
                  currency=F('side__unit'))

    waste_dict = {w.id: w for w in WasteType.objects
                  .filter(pk__in={tx['obj'] for tx in transactions_dicts})
                  .select_related('category')}

    transactions_units = []
    for tx in transactions_dicts:
        tx['waste'] = waste_dict[tx['obj']]

        transactions_units.append(WasteTransactionUnit(waste=tx['waste'], data_dict=tx))

    summary = TransactionSummary(transactions_units)

    return render(request, 'dashboard/bsi/bsu-report-detail.html', {
        'section': 'bsu',
        'summary': summary,
    })


@login_required
def bsu_edit(request, bsu_id):
    status_code = 200

    user = request.user
    bsu = BankSampahUnit.objects.filter(bsi__user=user).get(id=bsu_id)

    if request.method == 'POST':
        bsu_admin_form = UserCreationForm(request.POST, instance=bsu.user)
        bsu_data_form = BSUEditForm(request.POST, instance=bsu)

        admin_valid = bsu_admin_form.is_valid()
        data_valid = bsu_data_form.is_valid()

        if admin_valid or data_valid:
            if admin_valid:
                admin = bsu_admin_form.save()

            if data_valid:
                bsu = bsu_data_form.save()

            status_code = 201

        else:
            status_code = 400
    else:
        bsu_admin_form = UserCreationForm(instance=bsu.user)
        bsu_data_form = BSUEditForm(instance=bsu)

    context = {
        'bsu_id': bsu_id,
        'bsu_admin_form': bsu_admin_form,
        'bsu_data_form': bsu_data_form,
        'status_code': status_code,
        'section': 'bsu',
    }

    return render(request, 'dashboard/bsi/bsu-edit.html', context)


@login_required
def bsu_nasabah_service(request, bsu_id, nasabah_id=None):
    user = request.user
    bsu = BankSampahUnit.objects.get(user=user)
    data = None
    if bsu.id != bsu_id or nasabah_id is not None and Nasabah.objects.get(id=nasabah_id).bsu_id != bsu.id:
        data = {
            'status': 500,
            "message": "Internal error",
        }
    elif request.method == "GET":
        if nasabah_id is None:
            data = {
                "status": 200,
                "data": [nasabah for nasabah in Nasabah.objects.filter(bsu_id=bsu_id).values()]
            }
        else:
            data = nasabah_views.nasabah_api(request, nasabah_id)
    elif request.method == "POST" or request.method == "PUT":
        data = nasabah_views.nasabah_api(request, nasabah_id)
    else:
        data = {
            "status": 405,
            "message": "Bad request method"
        }

    return JsonResponse(data)


@login_required
def nasabah_form(request):
    user = request.user
    bsu = BankSampahUnit.objects.get(user=user)
    data = {
        "section": "nasabah",
        "bsu_id": bsu.id,
        "bsu_name": bsu.name,
    }
    return render(request, "dashboard/nasabah/nasabah-form.html", data)


@login_required
def nasabah_list(request):
    user = request.user
    bsu = BankSampahUnit.objects.get(user=user)
    data = {
        "section": "nasabah",
        "bsu_id": bsu.id,
    }
    return render(request, "dashboard/nasabah/nasabah-list.html", data)


@login_required
def nasabah_profile(request, nasabah_id):
    user = request.user
    bsu = BankSampahUnit.objects.get(user=user)
    nasabah = nasabah_views.get_nasabah_from_database(nasabah_id)

    if nasabah.bsu == bsu:
        data = nasabah_views.get_profile(nasabah)
        data["nasabah_service_url"] = '/dashboard/bsu/{}/nasabah/{}/'.format(data["bsu_id"], data["nasabah_id"])
        data["section"] = "nasabah"
        return render(request, "dashboard/nasabah/nasabah-profile.html", data)
    else:
        return HttpResponse(status=500)


@permission_required('account.add_banksampah')
def bsi_dashboard(request):
    return render(request, 'dashboard/bsi/dashboard.html', {'section': 'dashboard'})
