# Generated by Django 2.2.2 on 2019-06-12 09:18

from django.db import migrations
import djmoney.models.fields
import djmoney.settings


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0002_auto_20190527_1036'),
    ]

    operations = [
        migrations.AddField(
            model_name='banksampahinduk',
            name='currency',
            field=djmoney.models.fields.CurrencyField(choices=djmoney.settings.CURRENCY_CHOICES, default='IDR', max_length=3),
        ),
    ]
