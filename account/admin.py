from django.contrib import admin
from account.models import BankSampahInduk, BankSampahUnit, Nasabah


@admin.register(BankSampahInduk)
class BankSampahIndukAdmin(admin.ModelAdmin):
    def add_view(self, request, form_url='', extra_context=None):
        self.fields = ('name', 'user')
        return super().add_view(request, form_url, extra_context)

    def change_view(self, request, object_id, form_url='', extra_context=None):
        self.fields = None
        return super().change_view(request, object_id, form_url, extra_context)


@admin.register(BankSampahUnit)
class BankSampahUnitAdmin(admin.ModelAdmin):
    def add_view(self, request, form_url='', extra_context=None):
        self.fields = ('bsi', 'name', 'user')
        return super().add_view(request, form_url, extra_context)

    def change_view(self, request, object_id, form_url='', extra_context=None):
        self.fields = None
        return super().change_view(request, object_id, form_url, extra_context)


@admin.register(Nasabah)
class NasabahAdmin(admin.ModelAdmin):
    def add_view(self, request, form_url='', extra_context=None):
        self.fields = ('bsu', 'first_name', 'last_name', 'user')
        return super().add_view(request, form_url, extra_context)

    def change_view(self, request, object_id, form_url='', extra_context=None):
        self.fields = None
        return super().change_view(request, object_id, form_url, extra_context)
