from django.contrib.auth.models import User
from django.core.validators import RegexValidator, MaxLengthValidator
from django.db import models
from django.core.exceptions import ValidationError

from django_enumfield import enum
from djmoney.models.fields import CurrencyField
from djmoney.settings import CURRENCY_CHOICES
from phonenumber_field.modelfields import PhoneNumberField


class BankAccount(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.user.username


class BankSampah(BankAccount):
    name = models.CharField(max_length=128)
    est_year = models.SmallIntegerField(null=True)
    pic = models.CharField(max_length=128)
    contact = PhoneNumberField()
    address = models.TextField()
    decree_no = models.CharField(max_length=128)
    decree_issuer = models.CharField(max_length=128)

    class Meta:
        abstract = True

    def __str__(self):
        return self.name


class BankSampahInduk(BankSampah):
    currency = CurrencyField(choices=CURRENCY_CHOICES)


class BankSampahUnit(BankSampah):
    bsi = models.ForeignKey(BankSampahInduk, on_delete=models.CASCADE)
    deposit_cost = models.DecimalField(default=0.1, max_digits=5, decimal_places=4,
                                       validators=[MaxLengthValidator(1.0)])

    @property
    def bank(self):
        return self.bsi

    @property
    def currency(self):
        return self.bank.currency


class BankSampahOfficerType(enum.Enum):
    HEAD = 1
    SECRETARY = 2
    TREASURER = 3
    STAFF = 4

    labels = {
        HEAD: 'Ketua',
        SECRETARY: 'Sekretaris',
        TREASURER: 'Bendahara',
        STAFF: 'Anggota',
    }


class BankSampahOfficer(models.Model):
    bank_sampah = models.ForeignKey(BankSampahUnit, on_delete=models.CASCADE)
    position = enum.EnumField(BankSampahOfficerType)
    name = models.CharField(max_length=128)


class Nasabah(BankAccount):
    bsu = models.ForeignKey(BankSampahUnit, on_delete=models.CASCADE)
    first_name = models.CharField(
        max_length=128,
        validators=[
            RegexValidator(
                regex='^[a-zA-Z ]*$',
                message='First name only contains alphabetical character'
            )
        ])
    last_name = models.CharField(
        max_length=128,
        validators=[
            RegexValidator(
                regex='^[a-zA-Z ]*$',
                message='Last name only contains alphabetical character'
            )
        ])
    contact = PhoneNumberField(
        blank=True,
        null=True
    )
    rt = models.PositiveSmallIntegerField(
        blank=True,
        null=True)
    rw = models.PositiveSmallIntegerField(
        blank=True,
        null=True)
    address = models.TextField(
        blank=True,
        null=True,
        validators=[
            MaxLengthValidator(255)
        ])

    @property
    def bank(self):
        return self.bsu

    @property
    def currency(self):
        return self.bank.currency

    def full_clean(self, exclude=None, validate_unique=True):
        try:
            return super().full_clean(exclude=exclude, validate_unique=validate_unique)
        except ValidationError as e:
            self.user.delete()
            raise ValidationError(list(dict(e).values()))

    def __str__(self):
        return self.first_name + " " + self.last_name
