$(document).ready(function(){
	$('button.navbar-toggler').on('click', function(){
		$('.overlay').animate({width: "toggle"});
		$('#sidebar').animate({width: "toggle"});
	});

	$('button > i.fa-times').on('click', function(){
		$('#sidebar').animate({width: "toggle"});
		$('.overlay').animate({width: "toggle"});
	});

	$(window).on("scroll", function() {
	    if($(window).scrollTop() > 50) {
            $("nav").css('background-color', "#009867");
        } else {
			$("nav").css("background-color", "rgb(0,0,0,0)");
        }
    });
})
