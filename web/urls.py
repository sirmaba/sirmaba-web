from django.urls import path
from web import views

app_name = 'web'
urlpatterns = [
    path('', views.landing_page, name='landing_page'),
]
