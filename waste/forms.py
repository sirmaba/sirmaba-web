from django import forms
from waste.models import WasteCategory, WasteType


class WasteCategoryForm(forms.ModelForm):
    class Meta:
        model = WasteCategory
        fields = ('name', 'icon')
        labels = {
            'name': 'Nama Kategori',
            'icon': 'Foto Kategori',
        }

        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control'}),
            'icon': forms.ClearableFileInput(attrs={'accept': 'image/*'}),
        }


class WasteTypeForm(forms.ModelForm):
    class Meta:
        model = WasteType
        fields = ('name', 'unit', 'bsi_price', 'bsu_price', 'info', 'icon')
        labels = {
            'name': 'Nama Sampah',
            'unit': 'Satuan',
            'bsi_price': 'Harga BSI (Rp)',
            'bsu_price': 'Harga BSU (Rp)',
            'info': 'Keterangan',
            'icon': 'Foto Sampah',
        }

        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control'}),
            'unit': forms.TextInput(attrs={'class': 'form-control'}),
            'bsi_price': forms.NumberInput(attrs={'class': 'form-control'}),
            'bsu_price': forms.NumberInput(attrs={'class': 'form-control'}),
            'info': forms.Textarea(attrs={'class': 'form-control'}),
            'icon': forms.ClearableFileInput(attrs={'accept': 'image/*'}),
        }
