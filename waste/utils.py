from account.models import BankSampahInduk, BankSampahUnit
from waste.models import WasteType


def get_waste_queryset(account):
    bsi_id = account.id if BankSampahInduk.objects.filter(bankaccount_ptr=account).exists() else \
        account.bsi.id if BankSampahUnit.objects.filter(bankaccount_ptr=account).exists() else \
        account.bsu.bsi.id

    return WasteType.objects.filter(category__bsi_id=bsi_id)
