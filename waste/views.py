from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect, render

from account.models import BankSampahInduk
from waste.models import WasteCategory, WasteType
from waste.forms import WasteCategoryForm, WasteTypeForm


@login_required
def index(request):
    return categories(request)


@login_required
def categories(request):
    user = request.user

    bsi = BankSampahInduk.objects.get(user=user)
    waste_categories = WasteCategory.objects.filter(bsi=bsi)

    if request.method == 'POST':
        waste_category_form = WasteCategoryForm(request.POST, request.FILES)

        if waste_category_form.is_valid():
            waste_category = waste_category_form.save(commit=False)
            waste_category.bsi = bsi
            waste_category.save()

            waste_category_form = WasteCategoryForm()
        else:
            pass
    else:
        waste_category_form = WasteCategoryForm()

    context = {
        'waste_categories': waste_categories,
        'waste_category_form': waste_category_form,
        'section': 'kategori',
    }

    return render(request, 'waste/waste-category-list.html', context)


@login_required
def categories_edit(request, category_id):
    waste_category = WasteCategory.objects.get(id=category_id)

    if request.method == 'POST':
        waste_category_form = WasteCategoryForm(request.POST, request.FILES, instance=waste_category)

        if waste_category_form.is_valid():
            waste_category_form.save()
            return redirect("waste:category-list")
        else:
            pass
    else:
        waste_category_form = WasteCategoryForm(instance=waste_category)

    context = {
        'waste_category_form': waste_category_form,
        'waste_category_id': category_id,
        'section': 'kategori',
    }

    return render(request, 'waste/waste-category-form.html', context)


@login_required
def type_list(request, category_id):
    user = request.user

    bsi = BankSampahInduk.objects.get(user=user)
    waste_category = WasteCategory.objects.get(id=category_id)
    waste_types = waste_category.wastetype_set.all()

    if request.method == 'POST':
        waste_type_form = WasteTypeForm(request.POST, request.FILES)

        if waste_type_form.is_valid():
            waste_type = waste_type_form.save(commit=False)
            waste_type.category = waste_category
            waste_type.save()

            waste_type_form = WasteTypeForm()
        else:
            pass
    else:
        waste_type_form = WasteTypeForm()

    context = {
        'waste_category': waste_category,
        'waste_types': waste_types,
        'waste_type_form': waste_type_form,
        'waste_type_form_url_name': 'waste:type-list',
        'section': 'kategori',
    }

    return render(request, 'waste/waste-type-list.html', context)


@login_required
def type_detail(request, type_id):
    user = request.user

    bsi = BankSampahInduk.objects.get(user=user)
    waste_type = WasteType.objects.get(id=type_id, category__bsi=bsi)
    waste_category = WasteCategory.objects.get(id=waste_type.category.id)

    if request.method == 'POST':
        waste_type_form = WasteTypeForm(request.POST, request.FILES, instance=waste_type)

        if waste_type_form.is_valid():
            waste_type_form.save()
        else:
            pass
    else:
        waste_type_form = WasteTypeForm(instance=waste_type)

    context = {
        'waste_category': waste_category,
        'waste_type': waste_type,
        'waste_type_form': waste_type_form,
        'section': 'kategori',
    }

    return render(request, 'waste/waste-type-detail.html', context)


def type_form(request, category_id):
    waste_category = WasteCategory.objects.get(id=category_id)
    waste_type_form = WasteTypeForm()

    context = {
        'waste_type_form': waste_type_form,
        'waste_category': waste_category,
    }
    return render(request, 'waste/waste-type-form.html', context)
