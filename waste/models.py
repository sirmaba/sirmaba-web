from django.db import models
from account.models import BankSampahInduk


class WasteCategory(models.Model):
    def icon_save_path(instance, filename):
        return 'icons/waste-category/{id}-{filename}'.format(id=instance.id, filename=filename)

    bsi = models.ForeignKey(BankSampahInduk, on_delete=models.CASCADE)
    name = models.CharField(max_length=128)
    icon = models.ImageField(upload_to=icon_save_path, default='icons/waste-category/default.jpg')

    def __str__(self):
        return self.name


class WasteType(models.Model):
    def icon_save_path(instance, filename):
        return 'icons/waste-type/{id}-{filename}'.format(id=instance.id, filename=filename)

    category = models.ForeignKey(WasteCategory, on_delete=models.CASCADE)
    name = models.CharField(max_length=128)
    unit = models.CharField(max_length=32, default='kg')
    bsi_price = models.DecimalField(max_digits=20, decimal_places=2)
    bsu_price = models.DecimalField(max_digits=20, decimal_places=2)
    icon = models.ImageField(upload_to=icon_save_path, default='icons/waste-type/default.jpg')
    info = models.TextField()

    def __str__(self):
        return self.name
