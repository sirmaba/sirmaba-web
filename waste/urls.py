from django.urls import path
from waste import views

app_name = 'waste'
urlpatterns = [
    path('', views.index, name='index'),
    path('categories/', views.categories, name='category-list'),
    path('categories/<int:category_id>/', views.type_list, name='type-list'),
    path('categories/<int:category_id>/edit/', views.categories_edit, name='category-edit'),
    path('type/<int:type_id>', views.type_detail, name='type-detail'),
    path('type/form/<int:category_id>/', views.type_form, name='type-form'),
]
